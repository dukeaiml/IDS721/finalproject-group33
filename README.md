# FinalProject: LLMOps - Model Serving with Rust
### Team Members (Group33)
Xingyu Zhang(xz365), Shang Ma(sm957), Zhecheng Dong(zd59), and Yufei Zhan(yz858)

### Demo Link
[group33 demo](https://youtu.be/pUk1M8XQFug)


### Requirements
- [x] Obtain Open Source ML Model
- [x] Create Rust Web Service for Model Inferences
- [x] Containerize Service and Deploy to Kubernetes
- [x] Impkement CI/CD Pipeline
- [x] Monitoring and Metrics Collection
- [x] Demo Youtube video

## Rust Web Service for Model Inferences
This Rust-based Actix-web service provides sentiment analysis for text input, to be more specific, __compare which is more positive among two different input text__, using a pre-trained sentiment classification model from the Rust-BERT library and obtains the ability to monitor HTTP request metrics using Prometheus.

### Create A Rust Project
Firstly, create a new Rust project using Cargo with following command
```bash
cargo new project_name
```

### Open Source ML Model
A `DistilBERT model finetuned on SST-2` is implemented in this project to predict the binary sentiment for a sentence which is included in the module [rust_bert::pipelines::sentiment](https://docs.rs/rust-bert/latest/rust_bert/pipelines/sentiment/index.html).

The `rust-bert` library relies on the [tch](https://github.com/LaurentMazare/tch-rs) crate for bindings to the C++ Libtorch API and the [libtorch](https://pytorch.org/get-started/locally/) environment should be setup before using these bindings.

### Applied Dependencies ([Cargo.toml](./sentiment_analysis_service/Cargo.toml))
**`actix-web`**: Provide a powerful, pragmatic, and extremely fast web framework for Rust.

**`actix-web-prom`**: Integrates Prometheus metrics monitoring into Actix-web applications.

**`serde`**: A Rust library for serializing and deserializing Rust data structures efficiently.

**`rust-bert`**: A Rust library for natural language processing tasks, including sentiment analysis, using pre-trained models from the BERT architecture.

**`log`**: A Rust logging facade providing a single logging API that abstracts over the actual logging implementation.

**`prometheus`**: A monitoring and alerting toolkit originally built at SoundCloud, used for collecting and aggregating metrics from services and applications.

### Functions
- **Input Query Struct**: Defines a structure `InputQuery` to deserialize incoming JSON requests. It contains two field text1 and text2 representing the input texts for sentiment analysis and comparison.
  ```rust
  #[derive(Deserialize)]
  struct InputQuery {
      text1: String,
      text2: String
  }
  ```
- **Sentiment Comparison Handler**: Defines an asynchronous function `compare_sentiments` which compares sentiments between two input texts. Upon receiving a query with two texts, it utilizes a sentiment analysis model to predict their sentiments. The function then determines which text is more positive or negative based on polarity and score comparisons. The comparison result is presented in HTML format, highlighting the more positive and negative texts along with their respective sentiment polarities and scores.
  ```rust
  async fn compare_sentiments(correct_counter: web::Data<IntCounterVec>, query: web::Query<InputQuery>) -> impl Responder {
      let text1 = query.text1.clone();
      let text2 = query.text2.clone();

      info!("Received request with texts: '{}' and '{}'", text1, text2);
      
      let sentiment_classifier = SentimentModel::new(Default::default()).expect("Failed to load the model!");

      let sentiments = sentiment_classifier.predict(&[text1.as_str(), text2.as_str()]);
      let sentiment1 = &sentiments[0];
      let sentiment2 = &sentiments[1];

      let (positive_text, positive_sentiment, negative_text, negative_sentiment) = if sentiment1.polarity == SentimentPolarity::Positive && sentiment2.polarity == SentimentPolarity::Negative {
          (text1, sentiment1, text2, sentiment2)
      } else if sentiment1.polarity == SentimentPolarity::Negative && sentiment2.polarity == SentimentPolarity::Positive {
          (text2, sentiment2, text1, sentiment1)
      } else if sentiment1.polarity == SentimentPolarity::Positive && sentiment2.polarity == SentimentPolarity::Positive {
          if sentiment1.score > sentiment2.score {
              (text1, sentiment1, text2, sentiment2)
          } else {
              (text2, sentiment2, text1, sentiment1)
          }
      } else {
          if sentiment1.score > sentiment2.score {
              (text2, sentiment2, text1, sentiment1)
          } else {
              (text1, sentiment1, text2, sentiment2)
          }
      };

      let response = format!(" <h1> Sentiment Comparison Service <i>by Group 33</i> </h1> 
                              <h3> Comparison Result: </h3>
                              <p><b>The more <span style='color: green;'>positive</span> text is sentence:</b> '{}' <b>(Polarity: '{:?}'; Score: '{}')</b>.<p><br><br>
                              <p><b>The more <span style='color: red;'>negative</span> text is sentence:</b> '{}' <b>(Polarity: '{:?}'; Score: '{}')</b>.<p><br><br>", 
                              positive_text, positive_sentiment.polarity, positive_sentiment.score, negative_text, negative_sentiment.polarity, negative_sentiment.score);

      correct_counter.with_label_values(&["/compare_sentiments"]).inc();

      HttpResponse::Ok().content_type("text/html").body(response)
  }
  ```
- **Main Function**: The `main` function initializes the Actix-web server. It sets up Prometheus metrics, including a counter for tracking correct HTTP requests, creates an Actix-web application, registers the sentiment prediction handler for the /sentiment endpoint, and binds the server to listen on 0.0.0.0:8080
  ```rust
  #[actix_web::main]
  async fn main() -> std::io::Result<()> {
      let prometheus = PrometheusMetricsBuilder::new("api")
          .endpoint("/metrics")
          .build()
          .unwrap();

      let correct_total_opts = opts!("http_requests_correct_total", "Total number of correct HTTP requests.").namespace("api");
      let correct_total = IntCounterVec::new(correct_total_opts, &["endpoint"]).unwrap();
      prometheus.registry.register(Box::new(correct_total.clone())).unwrap();

      HttpServer::new(move || {
          App::new()
              .wrap(prometheus.clone())
              .app_data(web::Data::new(correct_total.clone()))
              .service(
                  web::resource("/compare_sentiments").to(compare_sentiments)
              )
      })
      .bind("0.0.0.0:8080")?
      .run()
      .await
  }
  ```

### Build and Test
Run following commands in the root directory of the project to build and test the function:
```bash
cargo build
cargo test
```
Test the function locally:
![local test](./pic/localtest.jpg)

## Containerize Service and Deploy to Kubernetes
### Dockerization
Basically, there are two stages included in the [Dockerfile](./sentiment_analysis_service/Dockerfile):
- Build Stage:
    - Rust Base Environment(Image)
    - Dependency Installation
    - Download and Extract Libtorch
    - Set Environment Variables
    - Copy Project Files
    - Build Rust Project
    - Run Tests (if needed)
  
- Final Stage:
    - Select Base Image
    - Dependency Installation
    - Copy Built Binary and Libtorch
    - Set Environment Variables
    - Command Execution

``` dockerfile
# Use an official Rust image as the builder stage
FROM rust:1.75 as builder

# Install necessary dependencies
RUN apt-get update && \
    apt-get install -y build-essential cmake curl openssl libssl-dev

# Download and extract libtorch
ARG LIBTORCH_URL=https://download.pytorch.org/libtorch/cu118/libtorch-cxx11-abi-shared-with-deps-2.0.0%2Bcu118.zip
RUN curl -L ${LIBTORCH_URL} -o libtorch.zip && \
    unzip libtorch.zip -d / && \
    rm libtorch.zip

# Set environment variables for libtorch
ENV LIBTORCH=/libtorch
ENV LD_LIBRARY_PATH=${LIBTORCH}/lib:$LD_LIBRARY_PATH

# Set working directory and copy the entire Rust project
WORKDIR /sentiment_analysis_service
COPY . .

# Build the project for release
RUN cargo clean
RUN cargo build --release

# Final stage
FROM debian:bookworm-slim

# Install necessary dependencies
RUN apt-get update && \
    apt-get install -y build-essential cmake curl openssl libssl-dev

# Copy the built binary and libtorch from the builder stage
COPY --from=builder /sentiment_analysis_service/target/release/sentiment_analysis_service .
COPY --from=builder /libtorch/ /libtorch/

# Set environment variables
ENV LIBTORCH=/libtorch
ENV LD_LIBRARY_PATH=${LIBTORCH}/lib:$LD_LIBRARY_PATH
ENV ROCKET_ADDRESS=0.0.0.0

# Specify the command to run the application
CMD ["./sentiment_analysis_service"]
```
- Build the Docker image
```bash
docker build -t your-docker-image:tag
```
- Push it to Dockerhub
```bash
docker login
docker push your-docker-image:tag
```
![](pic/pic1.png)

### Deployment
Google Cloud Kubernetes Engine are used in our project.
- Install kubectl and gcloud from:  https://cloud.google.com/sdk/docs/install?hl=zh-cn#linux
- Create a new cluster in Google Cloud Kubernetes

![](pic/pic3.png)

- Login and connect to Google Cloud Kubernetes
```bash
gcloud auth login
gcloud container clusters get-credentials <cluster name> --zone <zone name> --project <project id>
```
- Write Kubernetes yaml Configuration
```yml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: final-deployment
spec:
  replicas: 3
  selector:
    matchLabels:
      app: final
  template:
    metadata:
      labels:
        app: final
    spec:
      containers:
      - name: final
        image: ireneisme/sentiment_analysis_service
        ports:
        - containerPort: 8080

---
apiVersion: v1
kind: Service
metadata:
  name: final-service
spec:
  type: LoadBalancer
  ports:
  - port: 80
    targetPort: 8080
  selector:
    app: final

```
- Apply configuration
```bash
kubectl apply -f <yaml file>
```
- Monitor deployment and get the external ip address
```bash
kubectl get deployments
kubectl get services
```
![](pic/pic2.png)

- Visit from external ip address successfully

# CI/CD Pipeline

This repository contains the CI/CD pipeline configuration. The pipeline consists of two stages: `build` and `deploy`.

## Build Stage

In the `build` stage, the Docker image for the project is built and pushed to the Docker registry. This stage uses the Docker executor with Docker-in-Docker (DIND) service.

### Steps:
1. Docker login to the GitLab Container Registry using the provided CI/CD variables (`CI_REGISTRY_USER` and `CI_REGISTRY_PASSWORD`).
2. Build the Docker image with tag
3. Push the Docker image to the GitLab Container Registry.

## Deploy Stage

In the `deploy` stage, the Docker image is deployed to Google Kubernetes Engine (GKE). This stage uses the Google Cloud SDK image.

### Steps:
1. Extract the GKE service account key from the CI/CD variable `GKE_SERVICE_KEY` and store it in a JSON file (`gcloud-service-key.json`).
2. Authenticate with Google Cloud using the service account key.
3. Retrieve the credentials for your GKE cluster by specifying the cluster name, zone, and project ID.
4. Apply the Kubernetes configuration defined in `kubernetes.yaml` to the cluster.
5. Restart the deployment to apply changes.
6. Verify the deployment by listing deployments, pods, and services.

## Setting Up GKE Service Account Key

To set up the `GKE_SERVICE_KEY` variable:
1. Generate a service account key with the necessary permissions to access GKE.
2. Copy the JSON key content.
3. Go to your GitLab project settings and navigate to `Settings` > `CI/CD` > `Variables`.
4. Add a new variable named `GKE_SERVICE_KEY` and paste the JSON key content as the value.


# Metrics/Monitoring

We used Prometheus and GCloud dashboard to monitor our service. 

## Prometheus
We use the prometheus and actix-web-prom crates to help us monitor our Actix web service. It tracks metrics e.g. total number of (both correct & incorrect) HTTP requests and the duration of HTTP requests, in a fine-grained level where different endpoints were tallied separately.

![prometheus](pic/metrics1.png)
![prometheus](pic/metrics2.png)
![prometheus](pic/metrics3.png)

## GCloud dashboard

The GKE dashboard in GCloud console shows various metrics related to the Kuberntes workloads.
It includes:
1. CPU Request % Used (Top 5): the percentage of requested CPU resources being utilized by the top 5 containers in the Kubernetes cluster
2. Memory Request % Used (Top 5): the memory usage percentage against the memory requested for the top 5 coontainers.

![Gcloud](pic/GCloud.jpg)