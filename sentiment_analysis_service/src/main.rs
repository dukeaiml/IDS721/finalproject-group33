use actix_web::{web, App, HttpResponse, HttpServer, Responder};
use rust_bert::pipelines::sentiment::{SentimentModel, SentimentPolarity};
use serde::Deserialize;
use actix_web_prom::PrometheusMetricsBuilder;
use prometheus::{opts, IntCounterVec};
use log::{info};

#[derive(Deserialize)]
struct InputQuery {
    text1: String,
    text2: String
}

async fn compare_sentiments(correct_counter: web::Data<IntCounterVec>, query: web::Query<InputQuery>) -> impl Responder {
    let text1 = query.text1.clone();
    let text2 = query.text2.clone();

    info!("Received request with texts: '{}' and '{}'", text1, text2);
    
    // let mut texts = Vec::new();
    // texts.push(text1.clone().as_str());
    // texts.push(text2.clone().as_str());
    
    let sentiment_classifier = SentimentModel::new(Default::default()).expect("Failed to load the model!");

    let sentiments = sentiment_classifier.predict(&[text1.as_str(), text2.as_str()]);
    let sentiment1 = &sentiments[0];
    let sentiment2 = &sentiments[1];

    let (positive_text, positive_sentiment, negative_text, negative_sentiment) = if sentiment1.polarity == SentimentPolarity::Positive && sentiment2.polarity == SentimentPolarity::Negative {
        (text1, sentiment1, text2, sentiment2)
    } else if sentiment1.polarity == SentimentPolarity::Negative && sentiment2.polarity == SentimentPolarity::Positive {
        (text2, sentiment2, text1, sentiment1)
    } else if sentiment1.polarity == SentimentPolarity::Positive && sentiment2.polarity == SentimentPolarity::Positive {
        if sentiment1.score > sentiment2.score {
            (text1, sentiment1, text2, sentiment2)
        } else {
            (text2, sentiment2, text1, sentiment1)
        }
    } else {
        if sentiment1.score > sentiment2.score {
            (text2, sentiment2, text1, sentiment1)
        } else {
            (text1, sentiment1, text2, sentiment2)
        }
    };

    let response = format!(" <h1> Sentiment Comparison Service <i>by Group 33</i> </h1> 
                            <h3> Comparison Result: </h3>
                            <p><b>The more <span style='color: green;'>positive</span> text is sentence:</b> '{}' <b>(Polarity: '{:?}'; Score: '{}')</b>.<p><br><br>
                            <p><b>The more <span style='color: red;'>negative</span> text is sentence:</b> '{}' <b>(Polarity: '{:?}'; Score: '{}')</b>.<p><br><br>", 
                            positive_text, positive_sentiment.polarity, positive_sentiment.score, negative_text, negative_sentiment.polarity, negative_sentiment.score);

    correct_counter.with_label_values(&["/compare_sentiments"]).inc();

    HttpResponse::Ok().content_type("text/html").body(response)
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    let prometheus = PrometheusMetricsBuilder::new("api")
        .endpoint("/metrics")
        .build()
        .unwrap();

    let correct_total_opts = opts!("http_requests_correct_total", "Total number of correct HTTP requests.").namespace("api");
    let correct_total = IntCounterVec::new(correct_total_opts, &["endpoint"]).unwrap();
    prometheus.registry.register(Box::new(correct_total.clone())).unwrap();

    HttpServer::new(move || {
        App::new()
            .wrap(prometheus.clone())
            .app_data(web::Data::new(correct_total.clone()))
            .service(
                web::resource("/compare_sentiments").to(compare_sentiments)
            )
    })
    .bind("0.0.0.0:8080")?
    .run()
    .await
}
